import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  postListItems = [
    {
      title: "premier post",
      content: "b dqsd qsldqsdsqd qsablad",
      loveIts: 0,
      created_at: "01/01/2018"
    },
    {
      title: "second post",
      content: "blabla  dqsdqs dqsd qdqsd qsdqs ",
      loveIts: 0,
      created_at: "01/02/2018"
    },
    {
      title: "troisième post",
      content: "dqsdsq dqs dqsd qsd sdqs ",
      loveIts: 0,
      created_at: "01/03/2018"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
